class Assets {
  static final auchan = Asset("assets/auchan.png", "Auchan");
  static final metro = Asset("assets/metro.png", "Metro");
  static final rost = Asset("assets/rost.png", "Rost");
  static final sportmaster = Asset("assets/sportmaster.png", "Sportmaster");
  static final chudo = Asset("assets/svg/chudo.svg", "Chudo-Market");

  static List<Asset> get props => [auchan, metro, rost, sportmaster, chudo];
}

class Asset {
  final String path;
  final String name;

  Asset(this.path, this.name);
}
