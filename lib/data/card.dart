import 'package:hive/hive.dart';

part 'card.g.dart';

@HiveType(typeId: 1)
class Card extends HiveObject {
  @HiveField(0)
  String name;
  @HiveField(1)
  String code;
  @HiveField(2)
  int color;
  @HiveField(3)
  bool whiteText = false;
  @HiveField(4)
  String? icon;
  @HiveField(5)
  bool fromFile;

  Card(this.name, this.code, this.color, this.whiteText, this.icon,
      this.fromFile);

  @override
  String toString() =>
      '$name $code $color $whiteText $icon'; // Just for print()
}
