import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BarcodeViewArguments {
  final String data;

  BarcodeViewArguments(this.data);
}

class BarcodeView extends StatelessWidget {
  static const route = "/BarcodeView";
  const BarcodeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () async {
          await SystemChrome.setPreferredOrientations(
              [DeviceOrientation.portraitUp]);
          Navigator.of(context).pop();
        },
        child: Center(
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(38),
                  child: BarcodeWidget(
                    barcode: Barcode.code128(),
                    data: (ModalRoute.of(context)!.settings.arguments
                            as BarcodeViewArguments)
                        .data,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
