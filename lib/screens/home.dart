import 'dart:io';
import 'dart:math';

import 'package:card_holder/assets.dart';
import 'package:card_holder/data/card.dart';
import 'package:card_holder/screens/add.dart';
import 'package:card_holder/screens/show_card.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Card;
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

const uuid = Uuid();

class HomeScreen extends StatelessWidget {
  static const route = "/HomeScreen";
  const HomeScreen({Key? key}) : super(key: key);

  _toBarcode(BuildContext context, String barcodeData) async {
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
    await Navigator.of(context).pushNamed(BarcodeView.route,
        arguments: BarcodeViewArguments(barcodeData));
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Card holder'), actions: [
        PopupMenuButton(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            itemBuilder: (BuildContext ctx) => ['Help', 'About']
                .map((e) => PopupMenuItem(child: Text(e)))
                .toList())
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
            Navigator.of(context).pushNamed(AddNewCardScreen.route),
        child: const Icon(Icons.add),
      ),
      body: ValueListenableBuilder(
          valueListenable: Hive.box<Card>('cards').listenable(),
          builder: (context, Box<Card> box, widget) {
            return StaggeredGridView.countBuilder(
              physics: const BouncingScrollPhysics(),
              crossAxisCount: 4,
              itemCount: box.length,
              itemBuilder: (BuildContext context, idx) {
                final item = box.getAt(idx)!;
                return CupertinoContextMenu(
                  actions: [
                    if (item.icon == null)
                      CupertinoContextMenuAction(
                        child: const Text('Change color'),
                        onPressed: () async {
                          final _random = Random();
                          final _anotherRandomColor = Color.fromRGBO(
                              _random.nextInt(256),
                              _random.nextInt(256),
                              _random.nextInt(256),
                              1);
                          item.color = _anotherRandomColor.value;
                          await item.save();
                          Navigator.pop(context);
                        },
                      ),
                    if (item.icon == null)
                      CupertinoContextMenuAction(
                        child: const Text('Toggle text color'),
                        onPressed: () async {
                          item.whiteText = !item.whiteText;
                          await item.save();
                          Navigator.pop(context);
                        },
                      ),
                    if (item.icon != null)
                      CupertinoContextMenuAction(
                        child: const Text('Reset image'),
                        onPressed: () async {
                          item.icon = null;
                          await item.save();
                          Navigator.pop(context);
                        },
                      ),
                    CupertinoContextMenuAction(
                      child: const Text('Set cover image'),
                      onPressed: () async {
                        await showDialog(
                            context: context,
                            builder: (ctx) {
                              final assets = Assets.props
                                ..add(Asset("pick", "Pick yourself"));

                              return AlertDialog(
                                  content: SingleChildScrollView(
                                      child: Wrap(
                                children: List<Widget>.generate(
                                  assets.length,
                                  (idx) => ListTile(
                                    onTap: () async {
                                      if (assets[idx].path == "pick") {
                                        FilePickerResult? result =
                                            await FilePicker.platform.pickFiles(
                                                type: FileType.image);

                                        if (result != null) {
                                          File file =
                                              File(result.files.single.path!);
                                          Directory appDocDir =
                                              await getApplicationDocumentsDirectory();
                                          String appDocPath = appDocDir.path;
                                          final path = appDocPath +
                                              '/' +
                                              uuid.v4() +
                                              '.' +
                                              file.path.split('.').last;
                                          await file.copy(path);
                                          item.icon = path;
                                          item.fromFile = true;
                                          await item.save();
                                        }
                                      } else {
                                        item.fromFile = false;
                                        item.icon = assets[idx].path;
                                        await item.save();
                                      }

                                      Navigator.of(context).pop();
                                    },
                                    title: Text(assets[idx].name),
                                  ),
                                ),
                              )));
                            });
                        Navigator.pop(context);
                      },
                    ),
                    CupertinoContextMenuAction(
                      child: const Text('Delete'),
                      onPressed: () async {
                        await item.delete();
                        Navigator.pop(context);
                      },
                    ),
                  ],
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () => _toBarcode(context, item.code),
                      child: Stack(
                        children: [
                          if (item.icon != null && item.icon!.contains('.svg'))
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset(item.icon!),
                            )
                          else if (item.icon != null)
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                  child: item.fromFile
                                      ? Image.file(File(item.icon!))
                                      : Image.asset(item.icon!)),
                            ),
                          Container(
                            decoration: BoxDecoration(
                                border: item.icon != null ? Border.all() : null,
                                color: item.icon != null
                                    ? null
                                    : Color(item.color),
                                borderRadius: BorderRadius.circular(15)),
                            child: Center(
                              child: DefaultTextStyle(
                                style: TextStyle(
                                    color: item.whiteText
                                        ? Colors.white
                                        : Colors.black),
                                child: Text(
                                  item.icon != null ? "" : item.name,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
              staggeredTileBuilder: (int index) =>
                  StaggeredTile.count(2, index.isEven ? 2 : 1),
            );
          }),
    );
  }
}
