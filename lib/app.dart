import 'package:card_holder/data/card.dart';
import 'package:card_holder/screens/add.dart';
import 'package:card_holder/screens/home.dart';
import 'package:card_holder/screens/show_card.dart';
import 'package:flutter/material.dart' hide Card;
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  bool loading = true;
  @override
  void initState() {
    // TODO: implement initState
    loadData();
    super.initState();
  }

  Future loadData() async {
    await Hive.initFlutter();
    // await Hive.deleteBoxFromDisk('cards');
    Hive.registerAdapter(CardAdapter());
    await Hive.openBox<Card>('cards');
    setState(() {
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          AddNewCardScreen.route: (ctx) => const AddNewCardScreen(),
          BarcodeView.route: (ctx) => BarcodeView()
        },
        home: loading ? SizedBox() : HomeScreen());
  }
}
